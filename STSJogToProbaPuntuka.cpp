
#include <robotlib/APILaserTracker.h>
#include <asl/Log.h>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

/**
 * Sample of the laser tracker with basics movements, connect, jog, get real time Datac
 */

int main()
{
	int model;

	robotlib::APILaserTracker laser_tracker;
	laser_tracker.setVerbose(1);
	bool is_ok = laser_tracker.connect();
	ASL_LOG_I("Connection is %i", is_ok);
	is_ok = laser_tracker.waitToTaskCompletion(10);
	ASL_LOG_I("Task completed is %i", is_ok);
	laser_tracker.printTaskResultInfo();
	ofstream file;
	file.open("D:/robotlib/samples/api_laser_tracker/datuakSTSzaharra2.txt");
	unsigned long long time;
	asl::Vec3 position, rotation;
	bool isSTS = true;
	


	// Get accesory model if any
	model = laser_tracker.getAccessory();
	ASL_LOG_I("Found accessory is %s", model == 340 ? "STS" : "No accesory");
	// if STS
	if (model == 340)
	{
		// Get angles
		float ini_elevation, ini_azimuth;
		laser_tracker.getSTSJogAngle(ini_elevation, ini_azimuth);
		file << "First sts data: \n";
		file << "Azimuth = " << ini_azimuth << "\n";
		file << "Elevation = " << ini_elevation << "\n";
		file << "/////////////////////////// \n";
		laser_tracker.waitToTaskCompletion(4);

		//Pos1
		float sts_elevation1 = -90.4736, sts_azimuth1 = -3.4503;
		is_ok = laser_tracker.jogSTSTo(sts_elevation1, sts_azimuth1);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		float curr_sts_elevation, curr_sts_azimuth;
		laser_tracker.getSTSJogAngle(curr_sts_elevation, curr_sts_azimuth);
		file << "Second sts data: \n";
		file << "Azimuth = " << curr_sts_azimuth << "\n";
		file << "Elevation = " << curr_sts_elevation << "\n";
		file << "/////////////////////////// \n";

		laser_tracker.waitToTaskCompletion(4);

		
		// Absolute move
		float elevation = -3.1784;
		float azimuth = -3.778;
		is_ok = laser_tracker.jogTo(elevation, azimuth);
		ASL_LOG_I("Send Jog command is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion(10.f);
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		float curr_elevation, curr_azimuth;
		laser_tracker.getJogAngle(curr_elevation, curr_azimuth);
		file << "First laser data: \n";
		file << "Azimuth = " << curr_azimuth << "\n";
		file << "Elevation = " << curr_elevation << "\n";
		file << "/////////////////////////// \n";
		//Laser tracker angles 3
		laser_tracker.getSTSJogAngle(curr_sts_elevation, curr_sts_azimuth);
		file << "Third sts data: \n";
		file << "Azimuth = " << curr_sts_azimuth << "\n";
		file << "Elevation = " << curr_sts_elevation << "\n";
		file << "/////////////////////////// \n";
		// Start tracking
		is_ok = laser_tracker.startTracking();
		ASL_LOG_I("Start tracking is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion(10);
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();

		laser_tracker.waitToTaskCompletion(5);
		
		for (int i = 0; i < 10; i++) {
			     
			laser_tracker.getRTMeasurement(position, rotation, isSTS);
			
			laser_tracker.getSTSJogAngle(curr_sts_elevation, curr_sts_azimuth);
			file << "Azimuth = " << curr_azimuth << "\n";
			file << "Elevation = " << curr_elevation << "\n";
			file << "X = " << position.x << "\n";
			file << "Y = " << position.y << "\n";
			file << "Z =" << position.z << "\n";
			file << "Azimuth_sts = " << curr_sts_azimuth << "\n";
			file << "Elevation_sts = " << curr_sts_elevation << "\n";
			file << "Pitch = " << rotation.y << "\n";
			file << "Yaw = " << rotation.z << "\n";
			file << "Roll = " << rotation.x << "\n";
			file << "/////////////////////////// \n";
			
		}
				
		laser_tracker.getJogAngle(curr_elevation, curr_azimuth);
		file << "When finish laser data: \n";
		file << "Azimuth = " << curr_azimuth << "\n";
		file << "Elevation = " << curr_elevation << "\n";
		file << "/////////////////////////// \n";

		laser_tracker.getSTSJogAngle(curr_sts_elevation, curr_sts_azimuth);
		file << "When finish sts data: \n";
		file << "Azimuth = " << curr_sts_azimuth << "\n";
		file << "Elevation = " << curr_sts_elevation << "\n";
		file << "/////////////////////////// \n";

		//laser_tracker.waitToTaskCompletion(30);
		//Pos2
		/*float sts_elevation2 = -30, sts_azimuth2 = 0;
		is_ok = laser_tracker.jogSTSTo(sts_elevation2, sts_azimuth2);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation2, sts_azimuth2);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation2, sts_azimuth2);

		laser_tracker.waitToTaskCompletion(4);

		//Pos3
		float sts_elevation3 = -60, sts_azimuth3 = 0;
		is_ok = laser_tracker.jogSTSTo(sts_elevation3, sts_azimuth3);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation3, sts_azimuth3);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation3, sts_azimuth3);

		laser_tracker.waitToTaskCompletion(4);

		//Pos4
		float sts_elevation4 = -90, sts_azimuth4 = 0;
		is_ok = laser_tracker.jogSTSTo(sts_elevation4, sts_azimuth4);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation4, sts_azimuth4);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation4, sts_azimuth4);

		laser_tracker.waitToTaskCompletion(4);

		//Pos5
		float sts_elevation5 = -120, sts_azimuth5 = 0;
		is_ok = laser_tracker.jogSTSTo(sts_elevation5, sts_azimuth5);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation5, sts_azimuth5);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation5, sts_azimuth5);

		laser_tracker.waitToTaskCompletion(4);

		//Pos6
		float sts_elevation6 = 0, sts_azimuth6 = 30;
		is_ok = laser_tracker.jogSTSTo(sts_elevation6, sts_azimuth6);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation6, sts_azimuth6);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation6, sts_azimuth6);

		laser_tracker.waitToTaskCompletion(4);

		//Pos7
		float sts_elevation7 = 0, sts_azimuth7 = 60;
		is_ok = laser_tracker.jogSTSTo(sts_elevation7, sts_azimuth7);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation7, sts_azimuth7);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation7, sts_azimuth7);

		laser_tracker.waitToTaskCompletion(4);

		//Pos8
		float sts_elevation8 = 0, sts_azimuth8 = 90;
		is_ok = laser_tracker.jogSTSTo(sts_elevation8, sts_azimuth8);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation8, sts_azimuth8);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation8, sts_azimuth8);

		laser_tracker.waitToTaskCompletion(4);

		//Pos9
		float sts_elevation9 = 0, sts_azimuth9 = 120;
		is_ok = laser_tracker.jogSTSTo(sts_elevation9, sts_azimuth9);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation9, sts_azimuth9);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation9, sts_azimuth9);

		laser_tracker.waitToTaskCompletion(4);

		//Pos10
		float sts_elevation10 = -30, sts_azimuth10 = 60;
		is_ok = laser_tracker.jogSTSTo(sts_elevation10, sts_azimuth10);
		ASL_LOG_I("JogSTSTo send is %i", is_ok);
		is_ok = laser_tracker.waitToTaskCompletion();
		ASL_LOG_I("Task completed is %i", is_ok);
		laser_tracker.printTaskResultInfo();
		// Get angles
		laser_tracker.getSTSJogAngle(sts_elevation10, sts_azimuth10);
		ASL_LOG_I("STS angles are elevation %f and azimuth %f", sts_elevation10, sts_azimuth10);

		laser_tracker.waitToTaskCompletion(4);*/

		
	}

	// Disconnect
	file.close();
	is_ok = laser_tracker.disconnect();
	ASL_LOG_I("Disconnection is %i", is_ok);
}
