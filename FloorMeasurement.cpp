
#include <robotlib/APILaserTracker.h>
#include <asl/Log.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <asl/JSON.h>

using namespace asl;
using namespace std;

/**
 * Sample of the laser tracker with basics movements, connect, jog, get real time Datac
 */

int main()
{
	int model;

	APILaserTracker laser_tracker;
	laser_tracker.setVerbose(1);
	bool is_ok = laser_tracker.connect();
	ASL_LOG_I("Connection is %i", is_ok);
	is_ok = laser_tracker.waitToTaskCompletion(10);
	ASL_LOG_I("Task completed is %i", is_ok);
	laser_tracker.printTaskResultInfo();
	ofstream file;
	file.open("D:/robotlib/samples/api_laser_tracker/FloorMeasurement.txt");
	unsigned long long time;
	asl::Vec3 position, rotation;
	bool isSTS;
	float elevation = -24.95;
	float azimuth = 86.72;
	laser_tracker.jogTo(elevation, azimuth);
	laser_tracker.waitToTaskCompletion(10);
	laser_tracker.startTracking();
	for(int i = 1; i < 15; i++){
			
		ASL_LOG_I("Press 'ENTER' to save measurement");

		if (cin.get())
		{
			laser_tracker.getRTMeasurement(position, rotation, isSTS);
			laser_tracker.getJogAngle(elevation, azimuth);
			file << "////////////// \n";
			file << "Saved measurement is: " << i << "\n";
			file << "Elevation = " << elevation << "\n";
			file << "Azimuth = " << azimuth << "\n";
			file << "X = " << position.x << "\n";
			file << "Y = " << position.y << "\n";
			file << "Z = " << position.z << "\n";
			file << "////////////// \n";
		}

		ASL_LOG_I("Measurement saved");
	}

	// Disconnect
	file.close();
	is_ok = laser_tracker.disconnect();
	ASL_LOG_I("Disconnection is %i", is_ok);
}
